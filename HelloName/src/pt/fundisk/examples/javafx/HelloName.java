package pt.fundisk.examples.javafx;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class HelloName extends Application {
	private Stage primaryStage;
	private Scene scene1,  scene2;
	Label helloLabel2;
	TextField helloTextField;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage= primaryStage;
		// Stage Title
		primaryStage.setTitle("Hello ....");
		// Begin Scene1 definition
		Label helloLabel = setLabel1();
		setTextField1();
		Button helloButton = setButton1();
		setScene1(helloLabel, helloButton);
		// End Scene1 definition
		
		// Begin Scene2 definition
		setLabel2();
		Button helloButton2 = getButton2();
		setScene2(helloButton2);
		// End Scene2 definition
	
		// Add Scene to Stage
		primaryStage.setScene(scene1);
		// Show Stage
		primaryStage.show();
	}

	private Label setLabel1() {
		Label helloLabel = new Label();
		helloLabel.setText("Name!");
		helloLabel.setId("helloLabel");
		helloLabel.setPrefWidth(100);
		helloLabel.setPrefHeight(25);
		return helloLabel;
	}

	private void setTextField1() {
		helloTextField = new TextField();
		helloTextField.setId("helloTextFieldId");
		helloTextField.setPrefWidth(100);
		helloTextField.setPrefHeight(25);
	}

private Button setButton1() {
	Button helloButton = new Button();
	helloButton.setId("helloButtonId");
	helloButton.setText("Say Hello");
	helloButton.setPrefWidth(100);
	helloButton.setPrefHeight(25);
	helloButton.setOnAction(new EventHandler<ActionEvent>() {
		
		@Override
		public void handle(ActionEvent event) {
			helloLabel2.setText("Hello "+helloTextField.getText());
			primaryStage.setScene(scene2);
			
		}
	});
	
	return helloButton;
}

	private void setScene1(Label helloLabel, Button helloButton) {
		VBox helloNameLayout = new VBox();
		helloNameLayout.getChildren().add(helloLabel);
		helloNameLayout.getChildren().add(helloTextField);
		helloNameLayout.getChildren().add(helloButton);
		helloNameLayout.setAlignment(Pos.CENTER);
		scene1 = new Scene(helloNameLayout, 400, 200);
	}

	private void setLabel2() {
		helloLabel2 = new Label();
		helloLabel2.setId("helloLabel2");
		
		helloLabel2.setPrefHeight(25);
	}

	private void setScene2(Button helloButton2) {
		VBox helloNameLayout2 = new VBox();
		helloNameLayout2.getChildren().add(helloLabel2);
		helloNameLayout2.getChildren().add(helloButton2);
		scene2 = new Scene(helloNameLayout2, 400, 200);
	}

	private Button getButton2() {
		Button helloButton2 = new Button();
		helloButton2.setId("helloButtonId2");
		helloButton2.setPrefWidth(100);
		helloButton2.setPrefHeight(25);
		helloButton2.setText("Go Back");
		helloButton2.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				primaryStage.setScene(scene1);
			}
		});
		return helloButton2;
	}

}
