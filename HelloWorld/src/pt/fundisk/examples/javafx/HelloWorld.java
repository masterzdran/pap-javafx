package pt.fundisk.examples.javafx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class HelloWorld extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// Stage Title
		primaryStage.setTitle("Hello World");

		// Begin Scene definition
		Label helloLabel = new Label();
		helloLabel.setText("Hello World!");

		StackPane stackPane = new StackPane();
		stackPane.getChildren().add(helloLabel);

		Scene scene = new Scene(stackPane, 200, 200);
		// End Scene definition

		// Add Scene to Stage
		primaryStage.setScene(scene);
		// Show Stage
		primaryStage.show();
	}

}
